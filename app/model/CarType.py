from pydantic import BaseModel

class CarType(BaseModel):
    id: int
    marque: str
    modèle: str
    nbPortes: str
    moteur: str