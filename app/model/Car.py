from pydantic import BaseModel
from enum import Enum


class Car(BaseModel):
    id: int
    couleur: str 
    prixVente: int
    prixAchat: int
    options: list
    etat: str
    anneeConstruction: int
    placeParking: int
    dateArrive: str #pas > today    
    dateLivraisonPrevu: str
    salesEmployee: int
    previousOwner: int
    newOwner: int
    carType: int
    