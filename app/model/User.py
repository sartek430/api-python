from pydantic import BaseModel

class User(BaseModel):
    id: int
    name: str
    surname: str | None = None
    email: str | None = None
    passwordHash: str | None = None
    tel:str | None = None
    newsletter: str | None = None
    isClient: bool | None = None