#libs imports
from fastapi import FastAPI
from pydantic import BaseModel

#local imports
from routers import Cars,Users
from internal import auth



app = FastAPI()


@app.get("/", tags = ["hello"])
async def say_hello():
    return "Hello World!"


class User(BaseModel):
    id: int
    name: str


app.include_router(Cars.router, tags = ["cars"])
app.include_router(Users.router, tags = ["users"])
app.include_router(auth.router, tags = ["auth"])