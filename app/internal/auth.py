#libs imports
from fastapi import Depends, HTTPException,status,APIRouter
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm

from routers.Users import users


router = APIRouter()

def hash_password(password: str):
    return hashlib.sha256(f'{password}'.encode('utf-8')).hexdigest()

@router.post("/login")
async def login(form_data: OAuth2PasswordRequestForm = Depends()):
    for user in users:
        if user["name"] == form_data.username:
            if user["password_hash"] == hash_password(form_data.password):
                return {"access_token": user["name"]}
    raise HTTPException(status_code=400, detail="Incorrect username or password")

    return {"access_token": user.username, "token_type": "bearer"}