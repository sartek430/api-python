#local imports
from model.Car import Car

#libs imports
from fastapi import APIRouter, status, HTTPException, Response

#System imports

router = APIRouter()

cars = [{
    "id":1,
    "couleur": "rouge",
    "prixVente": 3500000,
    "prixAchat": 3000000,
    "options": ["cuir alcentara", "toit ouvrant", "siege chauffant", "climatisation"],
    "etat": "nul",
    "anneeConstruction": 2002,
    "placeParking": 12,
    "dateArrive": "2014/02/06",
    "dateLivraisonPrevu": "2015/10/22",
    "salesEmployee": 1,
    "previousOwner": 1,
    "newOwner": 2,
    "carType": 1
    }]


@router.get("/cars", responses={status.HTTP_204_NO_CONTENT: {}})
async def get_all_cars() -> list[Car]:
    if len(cars) == 0 :
        return Response(status_code = status.HTTP_204_NO_CONTENT)
    return cars


@router.get("/cars/{car_id}",responses={status.HTTP_204_NO_CONTENT: {}})
async def get_car_by_id(car_id: int):
    if len(cars) == 0 :
        return Response(status_code = status.HTTP_204_NO_CONTENT)
    filtered_list = list(filter(lambda x: x["id"] == car_id, cars))
    return filtered_list

@router.post("/cars", status_code = status.HTTP_201_CREATED)
async def create_car(car: Car):
    cars.append(car)
    return car

@router.delete("/cars/{car_id}")
async def put_car(car_id: int) -> Car: 
    deleted_car = list(filter(lambda x: x["id"] == car_id, cars))
    cars.remove(deleted_car)
    return deleted_car[0]

@router.put("/cars/{car_id}")
async def put_car(car_id: int, car: Car) -> Car:
    put_car = list(filter(lambda x: x["id"] == car_id, cars))
    cars.remove(put_car[0])
    cars.append(car)
    return put_car[0]