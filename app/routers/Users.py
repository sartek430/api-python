#local imports
from model.User import User

#libs imports
from fastapi import APIRouter, status, HTTPException, Response

#System imports

router = APIRouter()

users = [
    {"id": 1, "name": "test1", "password_hash": "", "password": "aebdd3170e08298f965e12f583fdb68db64bc2500fe13bcb08b602b21cc7a1fc"},
    {"id": 2, "name": "test2", "password_hash": "", "password": "467baa6c1a9337043bbf7837b4ab15022f8d5002c10947a844112ae988a5e910"},
    {"id": 1, "name": "test4", "password_hash": "", "password": "0478721f1106c2a631a90181bac7efc77767a3903eb9220687bff8a14e940fa7"},
    {"id": 3, "name": "test3", "password_hash": "", "password": "1adda322c21ae683bfafe7c4fa45f5b242d75126040b759340acc10092087c4f"}
]


@router.get("/users", responses={status.HTTP_204_NO_CONTENT: {}})
async def get_all_users() -> list[User]:
    if len(users) == 0 :
        return Response(status_code = status.HTTP_204_NO_CONTENT)
    return users


@router.get("/users/search", responses={status.HTTP_204_NO_CONTENT: {}})
async def search_users(name: str):
    if len(users) == 0 :
        return Response(status_code = status.HTTP_204_NO_CONTENT)
    return list(filter(lambda x: x["name"] == name, users))


@router.get("/users/{user_id}", responses={status.HTTP_204_NO_CONTENT: {}})
async def get_user_by_id(user_id: int, name: str | None = None):
    if len(users) == 0 :
        return Response(status_code = status.HTTP_204_NO_CONTENT)
    filtred_list = list(filter(lambda x: x["id"] == user_id, users))
    if name is not None:
        filtred_list = list(filter(lambda x: x["name"] == name, users))
    return filtred_list


@router.post("/users", status_code = status.HTTP_201_CREATED)
async def create_user(user: User) -> User:
    users.append(user)
    return user


@router.delete("/users/{user_id}")  
async def delete_user(user_id: int) -> User:
    deleted_user = list(filter(lambda x: x["id"] == user_id, users))
    users.remove(deleted_user[0])
    return deleted_user[0]

@router.put("/users/{user_id}")
async def put_user(user_id: int, user: User) -> User:
    put_user = list(filter(lambda x: x["id"] == user_id, users))
    users.remove(put_user[0])
    users.append(user)
    return put_user[0] 